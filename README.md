# 注意事项：

#### 声明：

一切基于Miao-Yunzai，本人不负责更新、回答问题，Miao-Yunzai及本仓库属于开源项目，禁止贩卖！！！

#### 注意：

需要ffmpeg 环境和 Python 环境或者其他东西都可以在Miao-Yunzai的**docker-compose.yaml**文件更改开启

容器创建完成所有映射的文件都在yunzai目录下，并非Miao-Yunzai原来的目录！！！

##
-------------------------

#### 为什么选择docker版？
<details><summary>展开/收起</summary>
开箱即用的Miao-Yunzai机器人，近乎一键安装，无需考虑 redis 和 nodejs 环境安装和依赖安装、项目更新等问题

可在任意支持 docker 的 amd64 架构或 arm64 架构的操作系统（包括Linux、Windows、MacOS等）上轻松运行

结合 docker 部署脚本，免除手动配置 python 环境和编译 ffmpeg 的烦恼

所有用户数据均已映射到主机，方便备份和迁移
</details>

##
-------------------------

#### 安装docker以及docker-compose
<details><summary>展开/收起</summary>

已经安装docker的直接跳过，但是要注意确保已经安装docker-compose

不知道有没有安装可以直接输入下面指令，看看有没有反应

```
docker-compose --help
```

注：软路由openwrt系统可以直接在软件包查看，没有就直接安装

#### Linux[centos/ubuntu等]
安装docker脚本
```
curl -fsSL get.docker.com -o get-docker.sh
sudo sh get-docker.sh --mirror Aliyun
```

安装docker-compose脚本(这里不保证最新版，可以自己去下载)
```
sudo curl -L "https://github.com/docker/compose/releases/download/v2.11.1/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
```

#### Windows 和 Mac

请参考以下文档进行安装：

Docker一从入门到实践：https://yeasy.gitbook.io/docker_practice/install/windows

官方文档 https://docs.docker.com/desktop/windows/install/

官方文档 https://docs.docker.com/desktop/mac/install/
</details>

##
-------------------------


# 使用方法

#### 下载Miao-Yunzai

要在docker运行Miao-Yunzai，先输入以下指令进行clone下载文件（二选一即可）

国内机器:
```
git clone https://gitee.com/yoimiya-kokomi/Miao-Yunzai.git
```
国外机器:
```
git clone https://github.com/yoimiya-kokomi/Miao-Yunzai.git
```

##
-------------------------


# 安装以及启动
##
<details><summary>展开/收起</summary>

先cd进入Miao-Yunzai文件夹
```
cd Miao-Yunzai
```

构建容器(注意：此过程可能非常慢长)
```
docker-compose up -d
```

容器ID查看(注意是数字和英文的组合)
```
docker ps -a
```

启动完成后进入 miao-yunzai 的容器(如果进不去就把miao-yunzai替换成你的容器ID)
```
docker exec -it miao-yunzai /bin/sh
```

给miao-plugin安装依赖，也可以补全其他插件依赖
```
git pull && pnpm install -P
```

在容器内部运行项目，进行登录验证
```
node app
```

验证完成后，按快捷键 Ctrl+D 退出容器，然后重启容器
```
docker-compose restart
```

查看最后的100行日志并持续输出日志
```
docker-compose logs -f --tail=100
```
</details>

##
-------------------------

# docker使用注意事项：

Linux快捷键：

Ctrl+D是退出容器

Ctrl+C是结束进程

关闭ssh(终端)窗口不会结束docker容器的进程，所以确保是正常运行后可以直接关掉ssh(终端)窗口

可以正常使用后请先在QQ给机器人发送“#更新”，以此保持最新版本

##
-------------------------

# 安装指令插件以及插件依赖

#### 指令安装(需要依赖请用这个方法)

进入 miao-yunzai 的容器(如果进不去就把miao-yunzai替换成你的容器ID)

```
docker exec -it miao-yunzai /bin/sh
```

然后就可以用指令安装插件和插件依赖了

#### 安装普通js插件

回到下载要运行时候的文件夹

直接丢yunzai\plugins\example文件夹就行

（注意：确保是适用于miao-yunzai版本的js插件）

##
-------------------------

# 插件

## 插件索引：

https://gitee.com/yhArcadia/Yunzai-Bot-plugins-index

## 插件推荐：(记得安装插件依赖，方法在上面)

Miao-Plugin(https://gitee.com/yoimiya-kokomi/miao-plugin)

xiaoyao-cvs-plugin(https://gitee.com/Ctrlcvs/xiaoyao-cvs-plugin)

py-plugin(https://gitee.com/realhuhu/py-plugin)

Guoba-Yunzai / Guoba-Plugin(https://gitee.com/guoba-yunzai/guoba-plugin)
